from app.application import run_app
import os


if __name__ == '__main__':
    os.environ['FLASK_ENV'] = 'DOCKER'
    app = run_app()
    app.run(host='0.0.0.0', port=5000)
