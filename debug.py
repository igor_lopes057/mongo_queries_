from app.application import run_app
import os


if __name__ == '__main__':
    os.environ['FLASK_ENV'] = 'LOCAL'
    app = run_app()
    app.run(debug=True, host='0.0.0.0', port=5000)
