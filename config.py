import os


basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DEBUG = False
    TESTING = False

class LocalConfig(Config):
    DEBUG = True
    TESTING = True
    DATABASE_URL = 'mongodb://localhost:28001/Database'

class DockerConfig(Config):
    DEBUG = False
    DATABASE_URL = 'mongodb://mongodb:27017/database'


config = {
    'default': Config,
    'local': LocalConfig,
    'docker': DockerConfig
}
