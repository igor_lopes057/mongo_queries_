# Flask with mongo and docker

A simple application using mongodb, flask and docker. Enjoy! (:

## On venv:
* How to run('DOCKER'):

```
docker-compose build
```
```
docker-compose up
```

* How to run('LOCAL'):

```
docker pull mongo:3.2
```
```
docker run -d -p 28001:27017 -e AUTH=no mongo:3.2
```
```
python3 debug.py
```

* Stop docker after:
```
docker ps
```
```
docker stop <container_name>
```

## Tests:
* Run(make sure docker is up (*docker run -d -p 28001:27017 -e AUTH=no mongo:3.2*)):
```
python3 -m unittest __tests__/system_test.py -v
```

* Run(make sure flask and docker are up (*python3 debug.py*) and (*docker run -d -p 28001:27017 -e AUTH=no mongo:3.2*)):

To install testcafe:
```
npm install -g testcafe
```

Run on all nav:
```
testcafe all __tests__/system.testcafe.js
```

Only on firefox:
```
testcafe firefox __tests__/system.testcafe.js
```

Only on chrome:
```
testcafe chrome __tests__/system.testcafe.js
```
