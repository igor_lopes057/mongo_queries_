from flask import render_template, redirect
from flask_pymongo import PyMongo
from __init__ import create_app
from config import config
import os


def run_app():
    app = create_app()
    if os.environ['FLASK_ENV'] == 'LOCAL':
        app.config.from_object(config['local'])
        app.config['MONGO_URI'] = app.config['DATABASE_URL']
    if os.environ['FLASK_ENV'] == 'DOCKER':
        app.config.from_object(config['docker'])
        app.config['MONGO_URI'] = app.config['DATABASE_URL']
    db = PyMongo(app)
    col = db.db.costumers
    set_routes(app, col)
    return app


def set_routes(app, col):
    @app.route('/', methods=['GET'])
    def index():
        count = col.count_documents({})
        data = col.find({}, {'_id': 0}).sort('_id')
        names = col.distinct('Nome')
        emails = col.distinct('Email')
        return render_template('index.html', data=data,
                                             count=count,
                                             names=names,
                                             emails=emails)

    @app.route('/pull', methods=['POST'])
    def pulling():
        data =[{
            'Nome': 'Mano Brown',
            'Email': 'negrodrama@outlook.com'
        },
        {
            'Nome': 'Amado Batista',
            'Email': 'modao@yahoo.com'
        },
        {
            'Nome': 'Thor',
            'Email': 'sonofodin@gmail.com'
        },
        {
            'Nome': 'Me',
            'Email': 'donthaveone@outlook.com'
        },
        {
            'Nome': 'Tony Stark',
            'Email': 'sodead@richpeople.com'
        },
        {
            'Nome': 'Batman',
            'Email': 'detective.detective@.com'
        }]
        col.insert_many(data)
        return redirect("/")

    @app.route('/delete', methods=['POST'])
    def deleting():
        col.drop()
        return redirect("/")
