from unittest import TestCase, main
from flask_pymongo import PyMongo
from app.application import set_routes
from __init__ import create_app
from config import config
from __tests__.helpers import create_data


app = create_app()


class TestMongoFlask(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.app = app
        cls.app.config.from_object(config['local'])
        cls.app.config['MONGO_URI'] = cls.app.config['DATABASE_URL']
        cls.app_context = cls.app.test_request_context()
        cls.app_context.push()
        cls.client = cls.app.test_client()
        db = PyMongo(cls.app)
        cls.col = db.db.costumers
        set_routes(cls.app, cls.col)

    @classmethod
    def tearDownClass(cls):
        cls.col.drop()

    def setUp(self):
        self.app = app
        db = PyMongo(self.app)
        self.col = db.db.costumers

    def tearDown(self):
        ...

    def test__index__access_page__should_return_200_ok(self):
        # FIXTURE

        # EXECUTE
        response = self.client.get('http://0.0.0.0:5000/')

        # EXPECTED
        expected_response = 200
        self.assertEqual(expected_response, response.status_code)

    def test__pulling__simulate_a_click_on_button_query__should_return_302_redirect(self):
        # FIXTURE

        # EXECUTE
        response = self.client.post('http://0.0.0.0:5000/pull')

        # EXPECTED
        expected_value = 302
        self.assertEqual(expected_value, response.status_code)

    def test__deleting__simulate_a_click_on_button_clear_all__should_return_302_redirect(self):
        # FIXTURE

        # EXECUTE
        response = self.client.post('http://0.0.0.0:5000/delete')

        # EXPECTED
        expected_value = 302
        self.assertEqual(expected_value, response.status_code)

    def test__pulling__quering_values_on_database__should_return_list_of_names_correctly(self):
        # FIXTURE
        self.client.post('http://0.0.0.0:5000/pull')

        # EXECUTE
        response = self.col.distinct('Nome')

        # EXPECTED
        expected_value = ['Mano Brown', 'Amado Batista', 'Thor', 'Me', 'Tony Stark', 'Batman']
        self.assertEqual(expected_value, response)

    def test__deleting__clear_all__should_drop_every_data_on_db(self):
        # FIXTURE
        create_data(col=self.col, name='Mano Brown', email='negrodrama@outlook.com')

        # EXECUTE
        self.client.post('http://0.0.0.0:5000/delete')

        # EXPECTED
        expected_value = 0
        count = self.col.count_documents({})
        self.assertEqual(expected_value, count)

    def test__create_data__function__should_return_data_correctly(self):
        # FIXTURE

        # EXECUTE
        create_data(col=self.col, name='me', email='none')

        # EXPECTED
        query = self.col.distinct('Nome')
        expected_value = ['me']
        self.assertEqual(expected_value, query)


if __name__ == '__main__':
    main()
