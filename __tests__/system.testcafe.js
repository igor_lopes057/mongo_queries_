import { Selector, wait } from 'testcafe';


fixture('Testing mongodb with flask...')
  .page('http://0.0.0.0:5000/#')
  .afterEach( async t => {
    const element = Selector('input[name="deleted"]')
    await t
      .click(element);
  });

test('should check page\'s title if matches as \'MongoDBzão!\'', async t => {
  const title = Selector('title');
  await t
    .expect(title.innerText).eql('MongoDBzão!');
});

test('should simulate a click on \'Query 6 results\' button', async t => {
  const element = Selector('input[name="requested"]');
  await t
    .click(element)
    .expect(Selector('li').withText('Mano Brown').exists).ok();
});

test('should simulate a click on \'Clear all\' button', async t => {
  const element = Selector('input[name="deleted"]')
  await t
    .click(element)
    .expect(Selector('ul').withText('Nenhum registro!').exists).ok();
});
